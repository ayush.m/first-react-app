import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './App.css';
import Header from './components/Header/Header';
import Player from './components/Player/Player';


class App extends Component {
  constructor(props){
    super(props);
    this.state={
      players: this.props.initialPlayers
    }
  }
  onScoreChange=(index,delta)=>{
    console.log(index,delta)
    this.state.players[index].score+=delta;
    this.setState(this.state);
  }
  render() {
    return (
      <div className="scoreboard">
        <Header title={this.props.title}/>

          {/* Using loops in jsx  */}
          {
            this.state.players.map((player,index)=>{
              return(
                <Player 
                  key={player.id} 
                  name={player.name} 
                  score={player.score} 
                  onScoreChange={(delta)=>{this.onScoreChange(index,delta)}}/>
              )
            })
          }
      </div>
    );
  }
}

App.propTypes = {
  title: PropTypes.string.isRequired,
  initialPlayers: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    score: PropTypes.number.isRequired
  })).isRequired
};
export default App;
